#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Methods for the bot functionality."""
import sys
import os
import traceback
from configparser import ConfigParser
from PIL import ImageFont, ImageDraw, Image
from tempfile import mkstemp
from typing import Tuple

from telegram import (Update, InlineKeyboardMarkup, InlineKeyboardButton,
                      InlineQueryResultCachedSticker, Bot, StickerSet, ChatAction)
from telegram.error import BadRequest
from telegram.ext import (CallbackContext, Dispatcher, CommandHandler, InlineQueryHandler,
                          MessageHandler, Filters)
from telegram.utils.helpers import mention_html
from emoji import emojize

config = ConfigParser()
config.read('bot.ini')

ADMIN: int = int(config['praedikat-besonders-bottig']['admins_chat_id'])
""":obj:`int`: Chat ID of the admin as read from ``bot.ini``."""
STICKER_SET_NAME: str = config['praedikat-besonders-bottig']['sticker_set_name']
""":obj:`str`: The name of the sticker set used to generate the sticker as read from
``bot.ini``."""
STICKERS_DIR: str = 'stickers'
""":obj:`str`: The directory, where the stickers are stored."""
TEMPLATE: str = '/'.join([STICKERS_DIR, 'template.png'])
""":obj:`str`: Path of the stickers template."""
FONT: str = '/'.join([STICKERS_DIR, 'Rotis-SemiSerif-Std-Bold.ttf'])
""":obj:`str`: Path of the stickers font."""
FONT_SIZE: int = 73
""":obj:`int`: Default font size."""
COLOR: Tuple[int, int, int] = (46, 55, 46)
"""tuple(:obj:`int`, :obj:`int`, :obj:`int`): Text color for the stickers as RGB value."""
HOMEPAGE: str = 'https://hirschheissich.gitlab.io/praedikat-besonders-bottig/'
""":obj:`str`: Homepage of this bot."""


def info(update: Update, context: CallbackContext) -> None:
    """
    Returns some info about the bot.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    text = emojize(
        ('I\'m <b>Prädikat besonders bottig</b>. My profession is generating custom stickers '
         ' based on the template of the »Deutsche Film- und Medienbewertung«.'
         '\n\nTo learn more about me, please visit my homepage '
         ':slightly_smiling_face:.'),
        use_aliases=True)

    keyboard = InlineKeyboardMarkup.from_button(
        InlineKeyboardButton(emojize('Prädikat besonders bottig :robot_face:', use_aliases=True),
                             url=HOMEPAGE))

    update.message.reply_text(text, reply_markup=keyboard)


def error(update: Update, context: CallbackContext):
    """
    Informs the originator of the update that an error occured and forwards the traceback to the
    admin.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    # Inform sender of update, that something went wrong
    if update.effective_message:
        text = emojize('Something went wrong :worried:. I informed the admin :nerd_face:.',
                       use_aliases=True)
        update.effective_message.reply_text(text)

    # Get traceback
    trace = ''.join(traceback.format_tb(sys.exc_info()[2]))

    # Gather information from the update
    payload = ''
    if update.effective_user:
        payload += ' with the user {}'.format(
            mention_html(update.effective_user.id, update.effective_user.first_name))
    if update.effective_chat:
        payload += f' within the chat <i>{update.effective_chat.title}</i>'
        if update.effective_chat.username:
            payload += f' (@{update.effective_chat.username})'
    if update.poll:
        payload += f' with the poll id {update.poll.id}.'
    text = f'Hey.\nThe error <code>{context.error}</code> happened{payload}. The full ' \
           f'traceback:\n\n<code>{trace}</code>'

    # Send to admin
    context.bot.send_message(ADMIN, text)

    # we raise the error again, so the logger module catches it.
    raise  # pylint: disable=E0704


def build_sticker_set_name(bot: Bot) -> str:
    """
    Builds the sticker set name given by ``STICKER_SET_NAME`` for the given bot.

    Args:
        bot: The Bot owning the sticker set.

    Returns:
        str
    """
    return '{}_by_{}'.format(STICKER_SET_NAME, bot.username)


def get_sticker_set(bot: Bot, name: str) -> StickerSet:
    """
    Get's the sticker set and creates it, if needed.

    Args:
        bot: The Bot owning the sticker set.
        name: The name of the sticker set.

    Returns:
        StickerSet
    """
    try:
        return bot.get_sticker_set(name)
    except BadRequest as e:
        if 'invalid' in str(e):
            with open('./stickers/template.png', 'rb') as sticker:
                bot.create_new_sticker_set(ADMIN, name, STICKER_SET_NAME, '🟡', png_sticker=sticker)
            return bot.get_sticker_set(name)
        else:
            raise e


def clean_sticker_set(bot: Bot) -> None:
    """
    Cleans up the sticker set, i.e. deletes all but the first sticker.

    Args:
        bot: The bot.
    """
    sticker_set = get_sticker_set(bot, build_sticker_set_name(bot))
    if len(sticker_set.stickers) > 1:
        for sticker in sticker_set.stickers[1:]:
            try:
                bot.delete_sticker_from_set(sticker.file_id)
            except BadRequest as e:
                if 'Stickerset_not_modified' in str(e):
                    pass
                else:
                    raise e


def get_sticker_id(predicate: str, context: CallbackContext) -> str:
    """
    Gives the sticker ID for the the requested sticker. If it was already build, just reads it from
    ``bot_data``. Builds a new sticker, otherwise.

    Args:
        predicate: The text to add to the template.
        context: The callback context as provided by the dispatcher.
    """
    # If sticker exists, just get the file id
    if predicate in context.bot_data:
        return context.bot_data[predicate]

    # Offsets
    y = 290
    left = 100

    # Prepare image
    image: Image = Image.open(TEMPLATE)
    draw = ImageDraw.Draw(image)

    defaut_font = ImageFont.truetype(FONT, FONT_SIZE)
    (_, max_height), _ = defaut_font.font.getsize('Tj')
    max_width = 512 - 2 * left

    # Calculate font size
    font_size = FONT_SIZE
    while True:
        current_font = ImageFont.truetype(FONT, font_size)
        (width, height), _ = current_font.font.getsize(predicate)
        if width > max_width or height > max_height:
            font_size = font_size - 1
        else:
            break

    # Calculate the y offset, if the font size was reduzed
    final_font = ImageFont.truetype(FONT, font_size)
    if font_size < FONT_SIZE:
        (_, font_height), _ = final_font.font.getsize('Tj')
        y_offset = (max_height - font_height) / 2
    else:
        y_offset = 0

    # Draw text centered
    x = (512 - width) / 2 - 1
    draw.text((x, y + y_offset), predicate, fill=COLOR, font=final_font)

    # Use a temporary file for each sticker so we don't override another sticker by accident
    temp_file, temp_file_path = mkstemp(suffix='.png')
    image.save(temp_file_path)

    sticker_set_name = build_sticker_set_name(context.bot)
    emojis = '🟡'
    get_sticker_set(context.bot, sticker_set_name)

    # Add sticker to sticker set, get the ID and delete it afterwards
    context.bot.add_sticker_to_set(ADMIN,
                                   sticker_set_name,
                                   emojis,
                                   png_sticker=open(temp_file_path, 'rb'))
    sticker_set = get_sticker_set(context.bot, sticker_set_name)
    sticker_id = sticker_set.stickers[-1].file_id

    os.close(temp_file)
    os.remove(temp_file_path)

    # Save file id in bot_data
    context.bot_data[predicate] = sticker_id

    # Return sticker ID
    return sticker_id


def inline(update: Update, context: CallbackContext):
    """
    Answers an inline query by providing the requested sticker.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    query = update.inline_query.query

    if not query:
        return
    else:
        file_id = get_sticker_id(query, context)

    update.inline_query.answer(
        [InlineQueryResultCachedSticker(id='prädikat', sticker_file_id=file_id)])
    clean_sticker_set(context.bot)


def message(update: Update, context: CallbackContext):
    """
    Answers a text message by providing the requested sticker.

    Args:
        update: The Telegram update.
        context: The callback context as provided by the dispatcher.
    """
    message = update.message
    text = message.text

    if not text:
        return

    if '\n' in text:
        message.reply_text(text='Sorry, I can\'t handle line breaks. I\'m but a simple bot ...')
        return

    context.bot.send_chat_action(update.effective_user.id, ChatAction.UPLOAD_PHOTO)

    file_id = get_sticker_id(text, context)
    message.reply_sticker(sticker=file_id)
    clean_sticker_set(context.bot)


def register_dispatcher(disptacher: Dispatcher) -> None:
    """
    Adds handlers and sets up jobs. Convenience method to avoid doing that all in the main script.

    Args:
        disptacher: The :class:`telegram.ext.Dispatcher`.
    """
    dp = disptacher

    # error handler
    dp.add_error_handler(error)

    # basic command handlers
    dp.add_handler(CommandHandler(['start', 'help'], info))

    # functionality
    dp.add_handler(InlineQueryHandler(inline))
    dp.add_handler(MessageHandler(Filters.text, message))
