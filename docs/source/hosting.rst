Hosting this bot
----------------

If you like the functionality of this bot and want to host your own instance, simply clone the
repository, plug in your bot token and chat id in ``bot.ini``. Also, you will need to add your bot
to a group, where the generated stickers can be posted. This is needed for retrieving their file
IDs. Add the chat ID of the group to ``bot.ini`` as well. Then, install the dependencies and you're ready to go!
