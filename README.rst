Praedikat besonders bottig
==========================

.. image:: https://img.shields.io/badge/python-3-blue
   :target: https://www.python.org/doc/versions/
   :alt: Supported Python versions

.. image:: https://img.shields.io/badge/backend-python--telegram--bot-blue
   :target: https://python-telegram-bot.org/
   :alt: Backend: python-telegram-bot

.. image:: https://img.shields.io/badge/documentation-is%20here-orange
   :target: https://hirschheissich.gitlab.io/praedikat-besonders-bottig
   :alt: Documentation

.. image:: https://img.shields.io/badge/uses%20the-FBW%20logo-orange
   :target: https://www.fbw-filmbewertung.com/
   :alt: FBW Logo

.. image:: https://img.shields.io/badge/chat%20on-Telegram-blue
   :target: https://t.me/PraedikatBesondersBot
   :alt: Telegram Chat

»Prädikat besonders bottig« is a Telegram Bot that allows you to build custom stickers based on the
template of the »`Deutsche Film- und Medienbewertung`_«. You can find it at `@PraedikatBesondersBot`_.

.. _`Deutsche Film- und Medienbewertung`: https://www.fbw-filmbewertung.com/
.. _`@PraedikatBesondersBot`: https://t.me/PraedikatBesondersBot
