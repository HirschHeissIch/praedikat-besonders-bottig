=========
Changelog
=========

Version 2.2
===========
*Released 2020-08-17*

Enhancements:

* Include the `ptbstats <https://hirschheissich.gitlab.io/ptbstats/>`_ plugin for statistics


Version 2.1
===========
*Released 2020-06-20*

Bug fixes:

* Make deletion of stickers from set more robust

Version 2.0
===========
*Released 2020-06-03*

Major changes:

* Now uses ``Bot.add_sticker_to_set`` to generate stickers. Fixes issue of Apple users not seeing the generated stickers
* Auto-reply introduced in v1.1 is therefore removed
* ``bot.ini`` must be provided with a sticker set name (defaults to ``StickerGenerationSet``)

Minor changes:

* Bot now sends ``ChatAction`` when building stickers in private chat to display progress


Version 1.1
===========
*Released 2020-05-21*

* Reply to stickers with sticker content. This is helpful for Apple users, who can't see the stickers.

Version 1.0
===========
*Released 2020-03-20*

**New features**

* Simple bot commands for showing info about the bot
* Create stickers dynamically